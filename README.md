# User Compatibility Pack for JIRA

## What it is?

Atlassian provides a compatibility library that your plugin can include so that it can interact with either v5 or v6 in a safe manner.

## When I need it?

* If you want to be compatible with JIRA 5.x you cannot use `ApplicationUser` class directly, as it was introduced in JIRA 5.1.1
* If you read values from `CustomFields` containing Users. In older versions `CustomFields` used to return `User` object, in 6.0 JIRA returns `ApplicationUser`. As in 6.0 these types are totally incompatible, you will probably get an exception:

    java.lang.ClassCastException: com.atlassian.jira.user.DelegatingApplicationUser cannot be cast to com.atlassian.crowd.embedded.api.User


## How to use it?

Add a maven dependency by adding following to your `pom.xml`:

    <dependency>
        <groupId>com.atlassian.usercompatibility</groupId>
        <artifactId>usercompatibility-jira</artifactId>
        <version>0.18</version>
    </dependency>

## How to contribute?

This library is licensed under the BSD License so you can easily contribute. You just have to use Apache Maven 3 for compiling.