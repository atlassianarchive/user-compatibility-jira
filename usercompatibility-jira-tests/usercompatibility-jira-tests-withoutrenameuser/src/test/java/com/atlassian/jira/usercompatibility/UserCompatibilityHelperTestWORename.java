package com.atlassian.jira.usercompatibility;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.user.MockUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.BuildUtilsInfo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertSame;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class UserCompatibilityHelperTestWORename
{

    @Mock
    private BuildUtilsInfo buildUtilsInfo;

    @Mock
    private UserManager userManager;

    private MockUser user;

    @Before
    public void setUp(){
        when(buildUtilsInfo.getApplicationBuildNumber()).thenReturn(800);

        MockComponentWorker worker = new MockComponentWorker();
        ComponentAccessor.initialiseWorker(worker);
        worker.addMock(BuildUtilsInfo.class, buildUtilsInfo);
        worker.addMock(UserManager.class, userManager);

        user = new MockUser("userName");
    }
    
    @Test(expected = ClassNotFoundException.class)
    public void testApplicationUserClassIsNotExisting() throws Exception{
        DelegatingApplicationUser.getAppUserClass();
    }

    @Test
    public void testConvertUserObject(){
        UserWithKey userWithKey = UserCompatibilityHelper.convertUserObject(user);
        assertEquals("username", userWithKey.getKey());
        assertSame(user, userWithKey.getUser());
    }


    @Test
    public void testGetKeyForUser(){
        String result = UserCompatibilityHelper.getKeyForUser(user);
        assertEquals("username", result);
    }


    @Test
    public void testGetUserForKey(){
        when(userManager.getUserObject("username")).thenReturn(user);
        User result = UserCompatibilityHelper.getUserForKey("username");
        assertSame(user, result);
    }

    @Test
    public void testGetUserObjectApplicableForCF(){
        Object result = UserCompatibilityHelper.getUserObjectApplicableForUserCF(user);
        assertSame(user, result);
    }

    @Test
    public void testIsUserObject(){
        Object nastyObject = new MockIssue(123L);

        assertTrue(UserCompatibilityHelper.isUserObject(user));
        assertFalse(UserCompatibilityHelper.isUserObject(nastyObject));
    }

}