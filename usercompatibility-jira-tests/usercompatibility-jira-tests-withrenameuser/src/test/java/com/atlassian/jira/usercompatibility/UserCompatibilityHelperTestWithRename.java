package com.atlassian.jira.usercompatibility;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockComponentContainer;
import com.atlassian.jira.mock.issue.MockIssue;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.MockUser;
import com.atlassian.jira.user.UserKeyService;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.BuildUtilsInfo;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertSame;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserCompatibilityHelperTestWithRename
{

    @Rule
    public MockComponentContainer container = new MockComponentContainer(this);

    @Mock
    @AvailableInContainer
    private BuildUtilsInfo buildUtilsInfo;

    @Mock
    @AvailableInContainer
    private UserKeyService userKeyService;

    @Mock
    @AvailableInContainer
    private UserManager userManager;

    @Before
    public void setUp(){
        //version with rename enabled
        when(buildUtilsInfo.getApplicationBuildNumber()).thenReturn(6041);
    }

    @Test
    public void testConvertUserObject(){
        MockApplicationUser user = new MockApplicationUser("userkey", "username");

        UserWithKey result = UserCompatibilityHelper.convertUserObject(user);

        assertEquals("userkey", result.getKey());
        assertSame(user.getDirectoryUser(), result.getUser());
    }

    @Test
    public void testGetKeyForUserForRegularUser(){
        User user = new MockUser("username");
        when(userKeyService.getKeyForUsername("username")).thenReturn("userkey");

        String result = UserCompatibilityHelper.getKeyForUser(user);

        assertEquals("userkey", result);
    }

    @Test
    public void testGetKeyForUserForApplicationUser(){
        MockApplicationUser user = new MockApplicationUser("userkey", "username");

        String result = UserCompatibilityHelper.getKeyForUser(user.getDirectoryUser());

        assertEquals("userkey", result);
    }

    @Test
    public void testGetUserObjectApplicableForCF(){
        User user = new MockUser("username");
        when(userKeyService.getKeyForUsername("username")).thenReturn("userkey");

        //result should be ApplicationUser in 6.0
        ApplicationUser result = (ApplicationUser)UserCompatibilityHelper.getUserObjectApplicableForUserCF(user);

        assertEquals("userkey", result.getKey());
        assertEquals(user, result.getDirectoryUser());
    }

    @Test
    public void testGetUserForKey(){
        MockApplicationUser user = new MockApplicationUser("userkey", "username");

        when(userManager.getUserByKey("userkey")).thenReturn(user);

        User result = UserCompatibilityHelper.getUserForKey("userkey");
        assertSame(user.getDirectoryUser(), result);

    }

    @Test
    public void testIsUserObject(){
        MockApplicationUser appUser = new MockApplicationUser("userkey", "username");
        MockUser user = new MockUser("username");
        Object strangeObject = new MockIssue(123L);

        assertTrue(UserCompatibilityHelper.isUserObject(appUser));
        assertTrue(UserCompatibilityHelper.isUserObject(user));
        assertFalse(UserCompatibilityHelper.isUserObject(strangeObject));
    }


}
