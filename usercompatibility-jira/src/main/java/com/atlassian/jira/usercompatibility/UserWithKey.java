package com.atlassian.jira.usercompatibility;

import com.atlassian.crowd.embedded.api.User;

/**
 * Represents a user with an appropriate "key" in a way that is compatible with both JIRA 5 and JIRA 6.
 *
 * In JIRA 5, the key is the lower-cased username. (it has to be lower-cased because usernames are case-insensitive).
 *
 * In JIRA 6, this key comes from the corresponding ApplicationUser object and remains constant even after a user's
 * username is edited.
 */
public interface UserWithKey
{

    /**
     * @return the user identified by key
     */
    User getUser();

    /**
     * @return the key which distinguishes the ApplicationUser as unique
     */
    String getKey();

    /**
     * Implementations must ensure equality based on getKey().
     *
     * @param obj object to compare to.
     * @return <code>true</code> if and only if the key matches.
     */
    boolean equals(Object obj);

    /**
     * Implementations must produce a hashcode based on getKey().
     *
     * @return hashcode.
     */
    int hashCode();
}
