package com.atlassian.jira.usercompatibility;

public class ApplicationUserUtilAccessException extends RuntimeException
{
    public ApplicationUserUtilAccessException()
    {
    }

    public ApplicationUserUtilAccessException(final String message)
    {
        super(message);
    }

    public ApplicationUserUtilAccessException(final String message, final Throwable cause)
    {
        super(message, cause);
    }

    public ApplicationUserUtilAccessException(final Throwable cause)
    {
        super(cause);
    }
}
