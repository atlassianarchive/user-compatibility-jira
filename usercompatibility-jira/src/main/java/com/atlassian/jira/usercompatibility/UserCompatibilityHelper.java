package com.atlassian.jira.usercompatibility;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.embedded.impl.IdentifierUtils;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import java.util.Collection;

/**
 * Helper class containing utility methods for plugins which supports both JIRA 6.0 and older. In this case, cannot use
 * directly a new ApplicationUser interface introduced in JIRA 6.0. This class contains methods which can be
 * used to perform type-safe operations.
 */
public class UserCompatibilityHelper
{

    private static final int JIRA_BUILD_NUMBER_6_0_MINIMUM_WITH_BREAKIT = 6040;

    /**
     * Converts given userObject into {@link UserWithKey}.
     * <p/>
     * This method was introduced as an answer for different return value from {@link
     * com.atlassian.jira.issue.fields.CustomField}s containing users in JIRA 6.0. In such case, {@link
     * com.atlassian.jira.issue.fields.CustomField} returns {@link User} in JIRA < 6.0, and ApplicationUser in
     * JIRA &gt;= 6.0
     *
     * @param userObject object representing User
     * @return {@link UserWithKey} object containg unique user's key and {@link User} object itself.
     * @throws IllegalArgumentException when given userObject has unknown type
     * @throws ApplicationUserUtilAccessException when necessary class/method cannot be accessed
     */
    public static UserWithKey convertUserObject(Object userObject)
    {
        if (userObject == null)
        {
            return null;
        }

        if (userObject instanceof User)
        {
            User user = (User) userObject;
            return new DelegatingUserWithKey(user, getKeyForUser(user));
        }

        if (!isRenameUserImplemented())
        {
            throw new IllegalArgumentException("Unknown class representing user: " + userObject.getClass().getName());
        }

        DelegatingApplicationUser appUser = new DelegatingApplicationUser(userObject);

        return new DelegatingUserWithKey(appUser.getDirectoryUser(), appUser.getKey());
    }

    /**
     * Returns unique key, which e.g. can be stored in persistent storage, for given {@link User}.
     * <p/>
     * For plugin which supports <b>only</b> JIRA >= 6.0 it is highly recommended to use
     * ApplicationUsers.getKeyFor(User) directly
     *
     * @param user the directory User
     * @return unique Key for given user
     * @throws ApplicationUserUtilAccessException when necessary class/method cannot be accessed
     */
    public static String getKeyForUser(User user)
    {
        if (user == null)
        {
            return null;
        }

        if (isRenameUserImplemented())
        {
            return DelegatingApplicationUsers.getKeyFor(user);
        }
        else
        {
            return IdentifierUtils.toLowerCase(user.getName());
        }
    }

    /**
     * Returns the object, which can be used as an input for custom fields based on users. This method can be safely
     * used in JIRA 6.0 and older. In order to populate desired customfield you have to call generic version of {@link
     * com.atlassian.jira.issue.customfields.CustomFieldType#updateValue(com.atlassian.jira.issue.fields.CustomField,
     * com.atlassian.jira.issue.Issue, Object)}.
     * <p/>
     * Example usage:
     * <pre>
     *     CustomFieldType userCFType = userCustomField.getCustomFieldType();
     *     User user = getUser();
     *     Object userForCF = UserCompatibilityHelper.getUserObjectApplicableForCF(user);
     *     userCFType.updateValue(userCustomField, issue, userForCF);
     * </pre>
     *
     * @throws ApplicationUserUtilAccessException when necessary class/method cannot be accessed
     */
    public static Object getUserObjectApplicableForUserCF(User user)
    {
        if (user == null)
        {
            return null;
        }

        if (!isRenameUserImplemented())
        {
            return user;
        }

        return DelegatingApplicationUsers.from(user);
    }

    /**
     * Returns the list of objects, which can be used as an input for custom fields based on multi-user's fields. This
     * method can be safely used in JIRA 6.0 and older. In order to populate desired customfield you have to call
     * generic version of {@link com.atlassian.jira.issue.customfields.CustomFieldType#updateValue(com.atlassian.jira.issue.fields.CustomField,
     * com.atlassian.jira.issue.Issue, Object)}.
     *
     * @throws ApplicationUserUtilAccessException when necessary class/method cannot be accessed
     */
    public static Collection<Object> getUserObjectsApplicableForMultiUserCF(Iterable<User> users)
    {
        if (users == null)
        {
            return null;
        }

        return ImmutableList.copyOf(Iterables.transform(users, convertUserToObjectApplicableForCF));
    }

    /**
     * Function which converts {@link User} into the object, which can be used as an input for custom fields based on users.
     * @see #getUserObjectApplicableForUserCF(com.atlassian.crowd.embedded.api.User)
     */
    public static final Function<User, Object> convertUserToObjectApplicableForCF = new Function<User, Object>()
    {
        @Override
        public Object apply(final User user)
        {
           return getUserObjectApplicableForUserCF(user);
        }
    };


    /**
     * Returns {@link User} object based on specified key.
     * <p/>
     * This method should be used as an opposite of {@link #getKeyForUser(com.atlassian.crowd.embedded.api.User)}
     * for finding users based on string, e.g. stored in persistent storage. For plugin which supports <b>only</b> JIRA
     * >= 6.0 it is highly recommended to use UserManager.getUserByKey(String) directly.
     * <p/>
     * <b>Warning: </b><br/> {@link com.atlassian.crowd.embedded.api.User#getName()} of returned user can be not equal
     * to given key.
     *
     * @param key User's key
     * @return {@link User} object for given key
     * @throws ApplicationUserUtilAccessException when necessary class/method cannot be accessed
     */
    public static User getUserForKey(String key)
    {
        if (key == null)
        {
            return null;
        }

        if (isRenameUserImplemented())
        {
            Object applicationUserObject = DelegatingApplicationUsers.byKey(key);

            if (applicationUserObject == null)
            { return null; }

            DelegatingApplicationUser appUser = new DelegatingApplicationUser(applicationUserObject);
            return appUser.getDirectoryUser();

        }
        else
        {
            // JRADEV-19986: I have deliberately changed to UserManager.getUserObject() to try to help compat with v4.4
            return ComponentAccessor.getUserManager().getUserObject(key);
        }
    }

    /**
     * Tells whether given object represents a user and can be passed to {@link #convertUserObject(Object)}.
     * <p/>
     * This method was introduced as an answer for different return value from {@link
     * com.atlassian.jira.issue.fields.CustomField}s containing users in JIRA 6.0. In such case, {@link
     * com.atlassian.jira.issue.fields.CustomField} returns {@link User} in JIRA < 6.0, and ApplicationUser in
     * JIRA &gt;= 6.0
     *
     * @param object object possibly representing a User
     * @return true if object can be passed to {@link #convertUserObject(Object)}.
     * @throws ApplicationUserUtilAccessException when necessary class/method cannot be accessed
     */
    public static boolean isUserObject(Object object)
    {
        try
        {
            return object instanceof User || (isRenameUserImplemented() && DelegatingApplicationUser.getAppUserClass().isInstance(object));
        }
        catch (ClassNotFoundException e)
        {
            throw new ApplicationUserUtilAccessException(e);
        }
    }

    private static boolean isRenameUserImplemented()
    {
        BuildUtilsInfo buildUtilsInfo = ComponentAccessor.getComponent(BuildUtilsInfo.class);
        // special consideration for any JIRA milestone which was not built off breakit
        // This is particularly important with 6.0-OD8 is live on JoD
        // In addition, using getApplicationBuildNumber() instead of buildUtilsInfo.getVersionNumbers() gives us back compatibility to v4.x
        return buildUtilsInfo.getApplicationBuildNumber() >= JIRA_BUILD_NUMBER_6_0_MINIMUM_WITH_BREAKIT;
    }

}
