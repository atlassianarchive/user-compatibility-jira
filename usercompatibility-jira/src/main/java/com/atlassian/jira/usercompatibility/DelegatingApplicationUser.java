package com.atlassian.jira.usercompatibility;

import com.atlassian.crowd.embedded.api.User;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Encapsulates reflection to talk to an ApplicationUser for 6.x versions of JIRA.
 *
 * This class is part of the internal implementation of UserCompatibilityHelper and not for direct usage by plugin developers.
 *
 * @since v0.11
 */
class DelegatingApplicationUser
{

    private final Object applicationUser;
    private final Class<?> appUsersClass;

    DelegatingApplicationUser(final Object applicationUser)
    {
        try
        {
            appUsersClass = getAppUserClass();
            if (!appUsersClass.isAssignableFrom(applicationUser.getClass()))
            {
                throw new IllegalArgumentException("Unknown class representing user: " + applicationUser.getClass().getName());
            }
            this.applicationUser = applicationUser;
        }
        catch (ClassNotFoundException e)
        {
            throw new ApplicationUserUtilAccessException(e);
        }

    }

    String getKey()
    {
        try
        {
            Method getKeyMethod = appUsersClass.getMethod("getKey");
            return (String) getKeyMethod.invoke(applicationUser);
        }
        catch (NoSuchMethodException e)
        {
            throw new ApplicationUserUtilAccessException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new ApplicationUserUtilAccessException(e);
        }
        catch (InvocationTargetException e)
        {
            throw new ApplicationUserUtilAccessException(e);
        }
    }

    User getDirectoryUser()
    {
        try
        {
            Method getKeyMethod = appUsersClass.getMethod("getDirectoryUser");
            return (User) getKeyMethod.invoke(applicationUser);
        }
        catch (NoSuchMethodException e)
        {
            throw new ApplicationUserUtilAccessException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new ApplicationUserUtilAccessException(e);
        }
        catch (InvocationTargetException e)
        {
            throw new ApplicationUserUtilAccessException(e);
        }
    }

    static Class<?> getAppUserClass() throws ClassNotFoundException
    {
        return Class.forName("com.atlassian.jira.user.ApplicationUser");
    }
}
