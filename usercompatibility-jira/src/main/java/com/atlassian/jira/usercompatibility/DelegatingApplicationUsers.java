package com.atlassian.jira.usercompatibility;

import com.atlassian.crowd.embedded.api.User;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Wrapper for {@code ApplicationUser} class that uses reflection for calling its methods.
 *
 * This class is part of the internal implementation of UserCompatibilityHelper and not for direct usage by plugin developers.
 *
 * @since v0.11
 */
class DelegatingApplicationUsers
{
    static String getKeyFor(User user)
    {
        try
        {
            Class<?> appUsersClass = getApplicationUsersClass();
            Method getAppUserKeyMethod = appUsersClass.getMethod("getKeyFor", User.class);
            return (String) getAppUserKeyMethod.invoke(null, user);

        }
        catch (NoSuchMethodException e)
        {
            throw new ApplicationUserUtilAccessException(e);
        }
        catch (InvocationTargetException e)
        {
            throw new ApplicationUserUtilAccessException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new ApplicationUserUtilAccessException(e);
        }
    }

    static Object byKey(String key)
    {
        try
        {
            Class<?> appUsersClass = getApplicationUsersClass();
            Method byKeyMethod = appUsersClass.getMethod("byKey", String.class);
            return byKeyMethod.invoke(null, key);

        }
        catch (NoSuchMethodException e)
        {
            throw new ApplicationUserUtilAccessException(e);
        }
        catch (InvocationTargetException e)
        {
            throw new ApplicationUserUtilAccessException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new ApplicationUserUtilAccessException(e);
        }
    }

    static Object from(User user){
        try
        {
            Class<?> appUsersClass = getApplicationUsersClass();
            Method fromMethod = appUsersClass.getMethod("from", User.class);
            return fromMethod.invoke(null, user);

        }
        catch (NoSuchMethodException e)
        {
            throw new ApplicationUserUtilAccessException(e);
        }
        catch (InvocationTargetException e)
        {
            throw new ApplicationUserUtilAccessException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new ApplicationUserUtilAccessException(e);
        }
    }


    private static Class<?> getApplicationUsersClass()
    {
        try
        {
            return Class.forName("com.atlassian.jira.user.ApplicationUsers");
        }
        catch (ClassNotFoundException e)
        {
            throw new ApplicationUserUtilAccessException(e);
        }
    }
}
