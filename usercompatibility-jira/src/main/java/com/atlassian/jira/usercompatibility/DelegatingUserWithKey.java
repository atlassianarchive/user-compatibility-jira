package com.atlassian.jira.usercompatibility;

import com.atlassian.crowd.embedded.api.User;
import com.google.common.base.Objects;

/**
 * Implementation of {@link UserWithKey} - plugin developers should just use the interface.
 *
 * @see UserWithKey
 */
public class DelegatingUserWithKey implements UserWithKey
{

    private final User user;
    private final String key;

    public DelegatingUserWithKey(User user, String key)
    {
        this.user = user;
        this.key = key;
    }

    public User getUser()
    {
        return user;
    }

    public String getKey()
    {
        return key;
    }


    @Override
    public boolean equals(final Object obj)
    {
        if (obj instanceof UserWithKey)
        {
            final UserWithKey other = (UserWithKey) obj;
            return Objects.equal(key, other.getKey());
        }
        return false;
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(key);
    }
}

